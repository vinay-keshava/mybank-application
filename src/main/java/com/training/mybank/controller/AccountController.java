package com.training.mybank.controller;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.training.mybank.dto.AccountDetails;
import com.training.mybank.dto.AccountSummaryResponse;
import com.training.mybank.service.AccountService;

import io.swagger.v3.oas.annotations.Operation;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/api/v1")
@RequiredArgsConstructor
public class AccountController {

	private final AccountService accountService;

	@GetMapping(value = "/{customerId}")
	@Operation(summary = "account summary")
	public ResponseEntity<List<AccountSummaryResponse>> getAccountDetails(@Valid @PathVariable long customerId) {
		return new ResponseEntity<>(accountService.getAccountDetails(customerId), HttpStatus.ACCEPTED);
	}
	
	@GetMapping(value = "/customer")
	@Operation(summary = "account details")
	public ResponseEntity<AccountDetails> retreiveTopTransactions(@Valid @RequestParam Long accountNumber) {
		return new ResponseEntity<>(accountService.retrieveTransactions(accountNumber), HttpStatus.OK);
	}
	

}
