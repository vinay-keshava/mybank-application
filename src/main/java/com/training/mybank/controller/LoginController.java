package com.training.mybank.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.training.mybank.dto.LoginDto;
import com.training.mybank.dto.ResponseDto;
import com.training.mybank.service.LoginService;

import io.swagger.v3.oas.annotations.Operation;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/api/v1")
@RequiredArgsConstructor
public class LoginController {

	private final LoginService loginService;

	@PostMapping("/login")
	@Operation(summary = "customer login")
	public ResponseEntity<ResponseDto> customerLogin(@Valid @RequestBody LoginDto loginDto) {
		return new ResponseEntity<>(loginService.login(loginDto), HttpStatus.OK);
	}
}
