package com.training.mybank.controller;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.training.mybank.dto.BeneficiaryAccountResponse;
import com.training.mybank.service.impl.BeneficiaryServiceImpl;

import io.swagger.v3.oas.annotations.Operation;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/api/v1")
@RequiredArgsConstructor
public class BeneficiaryController {

	private final BeneficiaryServiceImpl beneficiaryServiceImpl;

	@PostMapping("/customer/{customerId}/beneficiary")
	@Operation(summary = "get beneficiary details")
	public ResponseEntity<List<BeneficiaryAccountResponse>> getBeneficiaryDetails(@Valid @RequestParam Long accountNumber,
			@PathVariable Long customerId) {
		return new ResponseEntity<>(beneficiaryServiceImpl.getAllBeneficiary(accountNumber, customerId),
				HttpStatus.CREATED);
	}
}
