package com.training.mybank.controller;


import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.training.mybank.dto.FundTransferDto;
import com.training.mybank.dto.OtpVerification;
import com.training.mybank.dto.ResponseDto;
import com.training.mybank.service.FundTransferService;

import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/api/v1")
@RequiredArgsConstructor
public class FundTransferController {

	private final FundTransferService fundTransferService;

	@PostMapping("/customer/transactions")
	@Operation(summary = "fund transfer")
	public ResponseEntity<ResponseDto> fundTransfer(@RequestBody FundTransferDto dto) {
		return new ResponseEntity<>(fundTransferService.fundTransfer(dto), HttpStatus.CREATED);
	}
	
	@PutMapping("/otp")
	@Operation(summary = "verify transaction")
	public ResponseEntity<ResponseDto> verifyTransaction(@RequestBody OtpVerification otpVerification) {
		return new ResponseEntity<>(fundTransferService.validateOtp(otpVerification),HttpStatus.CREATED);
	}
}
