package com.training.mybank.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.training.mybank.entity.BeneficiaryAccount;

@Repository
public interface BeneficiaryAccountRepository extends JpaRepository<BeneficiaryAccount, Long> {
	List<BeneficiaryAccount> findAllByAccountAccountId(Long accountId);

}
