package com.training.mybank.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.training.mybank.entity.Transaction;

@Repository
public interface TransactionRepository extends JpaRepository<Transaction, Long>{

	List<Transaction> findTop10ByAccountNumber(Long accountNumber);

	Transaction findByAccountNumber(Long accountNumber);

	List<Transaction> findTop10ByAccountNumberOrderByTransactionDateDesc(Long accountNumber);

	
	List<Transaction> findAllByAccountNumber(Long accountNumber);

	
}
