package com.training.mybank.feing;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

import com.training.mybank.dto.OtpRequest;
import com.training.mybank.dto.OtpResponse;



@FeignClient(value = "fundTransfer-service",url ="https://prod-06.centralindia.logic.azure.com:443/workflows/a6cee14bdc1f4dfc95b1add34115b0e6/triggers/manual/paths/invoke?api-version=2016-06-01&sp=%2Ftriggers%2Fmanual%2Frun&sv=1.0&sig=wdV2KuUywfZIYjr_DajgjrUkRKveUkGnqk7uBoGbreU" )
public interface OtpFiegnClient {

	@GetMapping
	public OtpResponse getOtp(OtpRequest otpRequest);
}
