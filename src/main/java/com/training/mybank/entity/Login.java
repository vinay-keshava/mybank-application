package com.training.mybank.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.Id;
import jakarta.persistence.OneToOne;
import jakarta.validation.constraints.NotBlank;
import lombok.Data;

@Entity
@Data
public class Login {


	@Id
	private Long loginId;
	@NotBlank(message = "password is required")
	private String password;
	@Enumerated(EnumType.STRING)
	private LogInStatus logInStatus;
	
	@OneToOne
	private Customer customer;
}
