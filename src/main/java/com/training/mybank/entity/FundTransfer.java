package com.training.mybank.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.Data;

@Entity
@Data
public class FundTransfer {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long fundTransferId;
	
	private Long accountNumber;
	
	private Long beneficiaryAccountNumber;
	
	@Enumerated(EnumType.STRING)
	private TransactionType transactionType;
	
	private Double transactionAmount;
	

	private String otpToken;
	
	private String remarks;
	
	@Enumerated(EnumType.STRING)
	private FundTransferStatus fundTransferStatus; 

}
