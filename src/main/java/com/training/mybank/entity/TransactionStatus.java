package com.training.mybank.entity;

public enum TransactionStatus {
	PENDING, SUCCESS, FAILED
}
