package com.training.mybank.entity;

import java.time.LocalDateTime;

import org.hibernate.annotations.CreationTimestamp;

import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;
import lombok.Data;

@Entity
@Data
public class Transaction {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long transactionId;

	@Enumerated(EnumType.STRING)
	private TransactionType transactionType;

	@CreationTimestamp
	private LocalDateTime transactionDate;

	private Double transactionAmount;

	private Long accountNumber;

	@ManyToOne
	private FundTransfer fundTransfer;
}
