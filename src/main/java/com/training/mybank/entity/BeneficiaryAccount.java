package com.training.mybank.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;
import lombok.Data;

@Entity
@Data
public class BeneficiaryAccount {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long beneficiaryAccountId;

	private String accountHolderName;
	
	@Enumerated(EnumType.STRING)
	private AccountType accountType;

	private String code;
	
	private Long accountNumber;
	
	@ManyToOne
	private Account account;
}
