package com.training.mybank.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import org.springframework.stereotype.Service;

import com.training.mybank.dto.AccountDetails;
import com.training.mybank.dto.AccountSummaryResponse;
import com.training.mybank.dto.TransactionResponseDto;
import com.training.mybank.entity.Account;
import com.training.mybank.entity.Customer;
import com.training.mybank.entity.FundTransfer;
import com.training.mybank.entity.LogInStatus;
import com.training.mybank.entity.Transaction;
import com.training.mybank.exception.AccountNotFound;
import com.training.mybank.exception.CustomerNotFound;
import com.training.mybank.exception.CustomerNotLoggedIn;
import com.training.mybank.repository.AccountRepository;
import com.training.mybank.repository.CustomerRepository;
import com.training.mybank.repository.FundTransferRepository;
import com.training.mybank.repository.LoginRepository;
import com.training.mybank.repository.TransactionRepository;
import com.training.mybank.service.AccountService;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Service
@RequiredArgsConstructor
@Slf4j
public class AccountServiceImpl implements AccountService {

	final CustomerRepository customerRepository;
	final AccountRepository accountRepository;
	final TransactionRepository transactionRepository;
	final FundTransferRepository fundTransferRepository;
	final LoginRepository loginRepository;

	@Override
	public List<AccountSummaryResponse> getAccountDetails(long customerId) {

		Optional<Customer> customer = customerRepository.findById(customerId);
		if (customer.isEmpty()) {
			throw new CustomerNotFound("customer with this id not found");
		}
		List<Account> accounts = accountRepository.findByCustomerCustomerId(customerId);

		if (accounts.isEmpty()) {
			throw new AccountNotFound("Account not found for this customer");
		}
		
		List<AccountSummaryResponse> summaryResponses = new ArrayList<>();

		for (Account account : accounts) {
			AccountSummaryResponse response = new AccountSummaryResponse(customer.get().getFirstName(),
					account.getAccountType(), account.getAccountBalance(), account.getAccountNumber());
			summaryResponses.add(response);
		}
		return summaryResponses;

	}
	
	@Override
	public AccountDetails retrieveTransactions(Long accountNumber) {
 
		Account account = accountRepository.findByAccountNumber(accountNumber);
		
		if (Objects.isNull(account)) {
			log.warn("Account with accountId " + accountNumber + " Not Found ");
			throw new AccountNotFound("Account with accountId " + accountNumber + " not Found");
		}

		List<Transaction> transactions=transactionRepository.findTop10ByAccountNumberOrderByTransactionDateDesc(accountNumber);
		
		if (transactions.isEmpty()){
			log.warn("Account does not have any transactions");
			throw new AccountNotFound("Account does not have any transactions");
		}
		
	if(loginRepository.findByCustomerCustomerId(account.getCustomer().getCustomerId()).getLogInStatus().equals(LogInStatus.LOGGEDOUT)){
			log.warn("Customer Not logged In");
			throw new CustomerNotLoggedIn("Customer Not Logged In");
		}
		
		List<TransactionResponseDto> transactionResponseList = new ArrayList<>();
		for (Transaction transaction : transactions) {
		
			TransactionResponseDto responseDto = new TransactionResponseDto();
			responseDto.setAccountNumber(transaction.getAccountNumber());
			responseDto.setTransactionAmount(transaction.getTransactionAmount());
			responseDto.setTransactionDate(transaction.getTransactionDate());
			
			FundTransfer fundTransfer = fundTransferRepository.findByFundTransferId(transaction.getFundTransfer().getFundTransferId());
			responseDto.setRemarks(fundTransfer.getRemarks());
			responseDto.setTransactionType(transaction.getTransactionType());
			transactionResponseList.add(responseDto);	
		}
		
		AccountDetails accountDetails = new AccountDetails();
		accountDetails.setAccountbalance( account.getAccountBalance());
		accountDetails.setAccountCreationDate(account.getAccountCreationDate().toString());
		accountDetails.setAccountNumber(account.getAccountNumber());
		accountDetails.setTransactionList(transactionResponseList);
		return accountDetails;
		
	}

}
