package com.training.mybank.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import org.springframework.stereotype.Service;

import com.training.mybank.dto.BeneficiaryAccountResponse;
import com.training.mybank.entity.Account;
import com.training.mybank.entity.BeneficiaryAccount;
import com.training.mybank.entity.Customer;
import com.training.mybank.exception.AccountMisMatchException;
import com.training.mybank.exception.AccountNotFound;
import com.training.mybank.exception.BeneficiaryAccountNotFound;
import com.training.mybank.exception.CustomerNotFound;
import com.training.mybank.repository.AccountRepository;
import com.training.mybank.repository.BeneficiaryAccountRepository;
import com.training.mybank.repository.CustomerRepository;
import com.training.mybank.service.BeneficiaryService;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
@RequiredArgsConstructor
public class BeneficiaryServiceImpl implements BeneficiaryService {

	final BeneficiaryAccountRepository beneficiaryAccountRepository;
	final CustomerRepository customerRepository;
	final AccountRepository accountRepository;

	@Override
	public List<BeneficiaryAccountResponse> getAllBeneficiary(Long accountNumber, Long customerId) {

		Customer customer = customerRepository.findByCustomerId(customerId);
		if (Objects.isNull(customer)) {
			log.error("customer with id " + customerId + " not found");
			throw new CustomerNotFound("customer with id " + customerId + " not found");
		}

		Account account = accountRepository.findByAccountNumber(accountNumber);
		if (Objects.isNull(account))
			throw new AccountNotFound("Account Not Found");

		if (!account.getCustomer().getCustomerId().equals(customerId))
			throw new AccountMisMatchException("Account Mismatch Exception");

		List<BeneficiaryAccountResponse> beneficiaryAccountResponses = new ArrayList<>();
		List<BeneficiaryAccount> accountList = beneficiaryAccountRepository
				.findAllByAccountAccountId(account.getAccountId());

		if (accountList.isEmpty()) {

			log.error("No Beneficiary Accounts Found for the account " + account.getAccountId());
			throw new BeneficiaryAccountNotFound(
					"No Beneficiary Accounts Found for the account " + account.getAccountId());
		}
		for (BeneficiaryAccount beneficiaryAccount : accountList) {
			BeneficiaryAccountResponse accountResponse = new BeneficiaryAccountResponse();
			accountResponse.setAccountNumber(beneficiaryAccount.getAccountNumber());
			accountResponse.setBeneficiaryName(beneficiaryAccount.getAccountHolderName());
			beneficiaryAccountResponses.add(accountResponse);
		}
		return beneficiaryAccountResponses;
	}
}
