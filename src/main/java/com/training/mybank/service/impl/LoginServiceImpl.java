package com.training.mybank.service.impl;

import java.util.Objects;

import org.springframework.stereotype.Service;

import com.training.mybank.dto.LoginDto;
import com.training.mybank.dto.ResponseDto;
import com.training.mybank.entity.Customer;
import com.training.mybank.entity.LogInStatus;
import com.training.mybank.entity.Login;
import com.training.mybank.exception.CustomerAlreadyLoggedIn;
import com.training.mybank.exception.CustomerNotFoundException;
import com.training.mybank.exception.InvalidCredentialException;
import com.training.mybank.repository.CustomerRepository;
import com.training.mybank.repository.LoginRepository;
import com.training.mybank.service.LoginService;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
@RequiredArgsConstructor
public class LoginServiceImpl implements LoginService {

	final CustomerRepository customerRepository;
	final LoginRepository loginRepository;

	public ResponseDto login(LoginDto loginDto) {
		Customer customer = customerRepository.findByCustomerId(loginDto.customerId());

		if (Objects.isNull(customer)) {
			throw new CustomerNotFoundException("Customer not found");
		}
		Login login = loginRepository.findByCustomerCustomerId(customer.getCustomerId());

		if (!loginDto.password().equals(login.getPassword())) {
			log.error("Invalid Credential");
			throw new InvalidCredentialException("Invalid Credentials");
		}

		if (login.getLogInStatus().equals(LogInStatus.LOGGEDIN)) {
			log.warn("Customer with id" + customer.getCustomerId() + " already logged in ");
			throw new CustomerAlreadyLoggedIn("Customer with id " + customer.getCustomerId() + " already logged in ");
		}

		login.setLogInStatus(LogInStatus.LOGGEDIN);
		loginRepository.save(login);
		log.info("Customer Logged with ID" + customer.getCustomerId() + "in successfully");
		return new ResponseDto("Logged In successfully", 200);

	}

}
