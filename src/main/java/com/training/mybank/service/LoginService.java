package com.training.mybank.service;

import com.training.mybank.dto.LoginDto;
import com.training.mybank.dto.ResponseDto;

import jakarta.validation.Valid;

public interface LoginService {

	ResponseDto login(@Valid LoginDto loginDto);

}
