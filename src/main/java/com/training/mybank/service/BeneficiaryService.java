package com.training.mybank.service;

import java.util.List;

import com.training.mybank.dto.BeneficiaryAccountResponse;

public interface BeneficiaryService {

	List<BeneficiaryAccountResponse> getAllBeneficiary(Long accountNumber,Long customerId);

}
