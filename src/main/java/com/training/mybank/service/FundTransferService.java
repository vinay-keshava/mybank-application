package com.training.mybank.service;

import com.training.mybank.dto.FundTransferDto;
import com.training.mybank.dto.OtpVerification;
import com.training.mybank.dto.ResponseDto;

public interface FundTransferService {

	ResponseDto fundTransfer(FundTransferDto dto);

	ResponseDto validateOtp(OtpVerification otpVerification);
	

}
