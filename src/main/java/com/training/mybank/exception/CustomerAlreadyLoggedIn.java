package com.training.mybank.exception;

public class CustomerAlreadyLoggedIn extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	final String message;

	public CustomerAlreadyLoggedIn(String message) {
		super(message);
		this.message = message;
	}

}
