package com.training.mybank.exception;

public class AccountMisMatchException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	final String message;

	public AccountMisMatchException(String message) {

		super(message);
		this.message = message;
	}
}
