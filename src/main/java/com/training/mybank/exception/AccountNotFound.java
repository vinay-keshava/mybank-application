package com.training.mybank.exception;

public class AccountNotFound extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	final String message;
	public AccountNotFound(String message) {
		super(message);
		this.message = message;
	}
}
