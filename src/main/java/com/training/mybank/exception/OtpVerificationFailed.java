package com.training.mybank.exception;

public class OtpVerificationFailed extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	final String message;
	public OtpVerificationFailed(String message) {
		super();
		this.message = message;
	}
	
}
