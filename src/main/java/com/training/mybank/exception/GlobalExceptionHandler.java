package com.training.mybank.exception;

import java.util.HashMap;
import java.util.Map;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.training.mybank.dto.ResponseDto;

@RestControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {

	
	protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex
			) {
		Map<String, String> errors = new HashMap<>();
		ex.getBindingResult().getAllErrors().forEach(error -> {
			String fieldName = ((FieldError) error).getField();
			String message = error.getDefaultMessage();
			errors.put(fieldName, message);
		});
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errors);
	}

	@ExceptionHandler(BeneficiaryAccountNotFound.class)
	public ResponseEntity<String> handleBeneficiary(BeneficiaryAccountNotFound exception) {
		return ResponseEntity.status(HttpStatus.NOT_FOUND).body(exception.getMessage());

	}
	
	
	@ExceptionHandler(CustomerNotLoggedIn.class)
	public ResponseEntity<Object> handlennOTLoggedInException(CustomerNotLoggedIn exception) {
		return ResponseEntity.status(HttpStatus.CONFLICT)
				.body(new ResponseDto(exception.getMessage(), HttpStatus.CONFLICT.value()));
	}
	
	@ExceptionHandler(CustomerAlreadyLoggedIn.class)
	public ResponseEntity<Object> handleCustomerAlreadyLoggedInException(CustomerAlreadyLoggedIn exception) {
		return ResponseEntity.status(HttpStatus.CONFLICT)
				.body(new ResponseDto(exception.getMessage(), HttpStatus.CONFLICT.value()));
	}

	@ExceptionHandler(InvalidCredentialException.class)
	public ResponseEntity<Object> handleInvalidCredential(InvalidCredentialException exception) {
		return ResponseEntity.status(HttpStatus.CONFLICT)
				.body(new ResponseDto(exception.getMessage(), HttpStatus.CONFLICT.value()));
	}

	@ExceptionHandler(CustomerNotFound.class)
	public ResponseEntity<Object> handleCustomerNotFound(CustomerNotFound exception) {
		return ResponseEntity.status(HttpStatus.NOT_FOUND)
				.body(new ResponseDto(exception.getMessage(), HttpStatus.NOT_FOUND.value()));
	}
	@ExceptionHandler(CustomerNotFoundException.class)
	public ResponseEntity<Object> handleCustomerNotFound(CustomerNotFoundException exception) {
		return ResponseEntity.status(HttpStatus.NOT_FOUND)
				.body(new ResponseDto(exception.getMessage(), HttpStatus.NOT_FOUND.value()));
	}

	@ExceptionHandler(AccountNotFound.class)
	public ResponseEntity<Object> handleAccountNotFound(AccountNotFound exception) {
		return ResponseEntity.status(HttpStatus.NOT_FOUND)
				.body(new ResponseDto(exception.getMessage(), HttpStatus.NOT_FOUND.value()));
	}
	
	@ExceptionHandler(AccountMisMatchException.class)
	public ResponseEntity<Object> handleAccountMisMatch(AccountMisMatchException exception) {
		return ResponseEntity.status(HttpStatus.CONFLICT)
				.body(new ResponseDto(exception.getMessage(), HttpStatus.CONFLICT.value()));
	}
	
	@ExceptionHandler(InsufficientBalanceExcpetion.class)
	public ResponseEntity<Object> handleInsufficientBalance(InsufficientBalanceExcpetion exception) {
		return ResponseEntity.status(HttpStatus.CONFLICT)
				.body(new ResponseDto(exception.getMessage(), HttpStatus.CONFLICT.value()));
	}
}
