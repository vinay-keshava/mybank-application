package com.training.mybank.dto;

import com.training.mybank.entity.AccountType;

public record AccountSummaryResponse(

		String accountHolderName, AccountType accountType, Double accountBalance, Long accountNumber) {

	
}
