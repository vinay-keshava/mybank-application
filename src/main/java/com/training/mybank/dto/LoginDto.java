package com.training.mybank.dto;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;

public record LoginDto(
		
	@NotNull(message = "customerId is a mandatory field")	Long customerId,
		@NotBlank(message = "password is mandatory field") String password
		) {

}
