package com.training.mybank.dto;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AccountDetails {

	Long accountNumber;
	Double accountbalance;
	String accountCreationDate;
	List<TransactionResponseDto> transactionList;

}
