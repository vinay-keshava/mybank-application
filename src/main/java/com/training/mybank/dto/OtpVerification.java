package com.training.mybank.dto;

import lombok.Data;

@Data
public class OtpVerification {

	Long fundTransferId;
	String otp;
}
