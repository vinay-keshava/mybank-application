package com.training.mybank.dto;

import org.springframework.http.HttpStatus;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class ErrorResponse {
	HttpStatus httpStatus;
	String message;
}
