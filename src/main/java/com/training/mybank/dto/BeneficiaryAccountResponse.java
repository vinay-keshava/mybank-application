package com.training.mybank.dto;

import lombok.Data;

@Data
public class BeneficiaryAccountResponse{
	private Long accountNumber;
	private String beneficiaryName;

}
