package com.training.mybank.dto;

import com.training.mybank.entity.TransactionType;

import jakarta.validation.constraints.NotNull;
import lombok.Data;

@Data
public class FundTransferDto{
	@NotNull(message = "from accountNumber mandatory") Long fromAccountNumber;
	@NotNull(message = "to accountNumber mandatory") Long toAccountNumber;
	Double amount;
	TransactionType transactionType;
	

	
}
