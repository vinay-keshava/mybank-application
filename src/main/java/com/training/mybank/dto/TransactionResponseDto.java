package com.training.mybank.dto;

import java.time.LocalDateTime;

import com.training.mybank.entity.TransactionType;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class TransactionResponseDto {

	private Long accountNumber;
	private Double transactionAmount;
	private String remarks;
	private LocalDateTime transactionDate;
	private TransactionType transactionType;
}
