package com.training.mybank.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class OtpResponse {

	 String otp;
}
