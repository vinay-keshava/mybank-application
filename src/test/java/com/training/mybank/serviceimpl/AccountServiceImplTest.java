package com.training.mybank.serviceimpl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.anyLong;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.training.mybank.dto.AccountDetails;
import com.training.mybank.dto.AccountSummaryResponse;
import com.training.mybank.dto.TransactionResponseDto;
import com.training.mybank.entity.Account;
import com.training.mybank.entity.AccountType;
import com.training.mybank.entity.Customer;
import com.training.mybank.entity.FundTransfer;
import com.training.mybank.entity.FundTransferStatus;
import com.training.mybank.entity.LogInStatus;
import com.training.mybank.entity.Login;
import com.training.mybank.entity.Transaction;
import com.training.mybank.exception.AccountNotFound;
import com.training.mybank.exception.CustomerNotFound;
import com.training.mybank.exception.CustomerNotLoggedIn;
import com.training.mybank.repository.AccountRepository;
import com.training.mybank.repository.CustomerRepository;
import com.training.mybank.repository.FundTransferRepository;
import com.training.mybank.repository.LoginRepository;
import com.training.mybank.repository.TransactionRepository;
import com.training.mybank.service.impl.AccountServiceImpl;

@ExtendWith(SpringExtension.class)
class AccountServiceImplTest {
	@Mock
	AccountRepository accountRepository;

	@Mock
	CustomerRepository customerRepository;

	@InjectMocks
	AccountServiceImpl accountServiceImpl;

	@Mock
	TransactionRepository transactionRepository;

	@Mock
	LoginRepository loginRepository;
	
	@Mock
	FundTransferRepository fundTransferRepository;

	@Test
	void testGetAccountDetailsForCustomerNotFound() {
		Mockito.when(customerRepository.findById(anyLong())).thenReturn(Optional.empty());
		;
		assertThrows(CustomerNotFound.class, () -> {
			accountServiceImpl.getAccountDetails(1);
		});

	}

	@Test
	void testGetAccountDetailsForEmptyAccount() {
		Customer customer = new Customer();
		customer.setCustomerId(Long.valueOf(1));
		customer.setFirstName("darshan");
		customer.setEmail("darshan@gmail.com");

		List<Account> accounts = new ArrayList<>();
		Mockito.when(customerRepository.findById(anyLong())).thenReturn(Optional.of(customer));
		Mockito.when(accountRepository.findByCustomerCustomerId(anyLong())).thenReturn(accounts);

		assertThrows(AccountNotFound.class, () -> {
			accountServiceImpl.getAccountDetails(1);
		});

	}

	@Test
	void testGetAccountDetailsSucess() {
		Customer customer = new Customer();
		customer.setCustomerId(Long.valueOf(1));
		customer.setFirstName("darshan");
		customer.setEmail("darshan@gmail.com");

		List<Account> accounts = new ArrayList<>();

		Account account1 = new Account();
		account1.setAccountNumber(Long.valueOf(122434));
		account1.setCustomer(customer);
		account1.setAccountBalance(Double.valueOf(200));
		account1.setAccountType(AccountType.CURRENT);

		Account account2 = new Account();
		account2.setAccountNumber(Long.valueOf(12324));
		account2.setCustomer(customer);
		account2.setAccountBalance(Double.valueOf(200));
		account2.setAccountType(AccountType.SAVINGS);

		accounts.add(account2);
		accounts.add(account1);

		AccountSummaryResponse accountSummaryResponse2 = new AccountSummaryResponse(customer.getFirstName(),
				account2.getAccountType(), account2.getAccountBalance(), account2.getAccountNumber());

		List<AccountSummaryResponse> accountSummaryResponses = new ArrayList<>();
		accountSummaryResponses.add(accountSummaryResponse2);
		accountSummaryResponses.add(accountSummaryResponse2);

		Mockito.when(customerRepository.findById(anyLong())).thenReturn(Optional.of(customer));
		Mockito.when(accountRepository.findByCustomerCustomerId(anyLong())).thenReturn(accounts);
		List<AccountSummaryResponse> responses = accountServiceImpl.getAccountDetails(1);

		assertEquals(accountSummaryResponses.size(), responses.size());

	}

	@Test
	void testretrieveTransactionForAccountNotFound() {
		Customer customer = new Customer();
		customer.setCustomerId(Long.valueOf(1));
		customer.setFirstName("vinay");
		customer.setEmail("k.vinay@hcl.com");

		Account account = new Account();
		account.setCustomer(customer);
		account.setAccountBalance(2000.00);
		account.setAccountNumber(12345L);

		Mockito.when(accountRepository.findByAccountNumber(anyLong())).thenReturn(null);
		assertThrows(AccountNotFound.class, () -> {
			accountServiceImpl.retrieveTransactions(12345L);
		});

	}

	@Test
	void testtestretrieveTransactionForNoTransaction() {
		Customer customer = new Customer();
		customer.setCustomerId(Long.valueOf(1));
		customer.setFirstName("vinay");
		customer.setEmail("k.vinay@hcl.com");

		Account account = new Account();
		account.setCustomer(customer);
		account.setAccountBalance(Double.valueOf(2000));
		account.setAccountNumber(Long.valueOf(12345));

		Mockito.when(accountRepository.findByAccountNumber(anyLong())).thenReturn(account);
		Mockito.when(transactionRepository.findByAccountNumber(anyLong())).thenReturn(null);
		Assertions.assertThrows(AccountNotFound.class, () -> {
			accountServiceImpl.retrieveTransactions(12345L);
		});

	}

	@Test
	void testtestretrieveTransactionForCustomerNotLoggedIN() {
		Customer customer = new Customer();
		customer.setCustomerId(Long.valueOf(1));
		customer.setFirstName("vinay");
		customer.setEmail("k.vinay@hcl.com");

		Account account = new Account();
		account.setCustomer(customer);
		account.setAccountBalance(Double.valueOf(2000));
		account.setAccountNumber(Long.valueOf(12345));

		Login login = new Login();
		login.setLogInStatus(LogInStatus.LOGGEDOUT);

		List<Transaction> transactions = new ArrayList<>();

		Transaction transaction1 = new Transaction();
		transaction1.setAccountNumber(Long.valueOf(12345));
		Transaction transaction2 = new Transaction();
		transaction2.setAccountNumber(Long.valueOf(12345));
		transactions.add(transaction1);
		transactions.add(transaction2);

		Mockito.when(transactionRepository.findTop10ByAccountNumberOrderByTransactionDateDesc(anyLong()))
				.thenReturn(transactions);

		Mockito.when(accountRepository.findByAccountNumber(anyLong())).thenReturn(account);
		Mockito.when(transactionRepository.findByAccountNumber(anyLong())).thenReturn(transaction1);
		Mockito.when(loginRepository.findByCustomerCustomerId(anyLong())).thenReturn(login);
		Assertions.assertThrows(CustomerNotLoggedIn.class, () -> {
			accountServiceImpl.retrieveTransactions(12345L);
		});
	}

	@Test
	void testtestretrieveTransactionSuccess() {
		Customer customer = new Customer();
		customer.setCustomerId(Long.valueOf(1));
		customer.setFirstName("vinay");
		customer.setEmail("k.vinay@hcl.com");
		customer.setContactNumber(91083113399L);

		Account account = new Account();
		account.setCustomer(customer);
		account.setAccountBalance(2000.00);
		account.setAccountNumber(12345L);
		account.setAccountCreationDate(LocalDateTime.now());
		

		FundTransfer fundTransfer = new FundTransfer();
		fundTransfer.setAccountNumber(account.getAccountNumber());
		fundTransfer.setFundTransferStatus(FundTransferStatus.PENDING);
		fundTransfer.setRemarks("Transaction successfull");
		fundTransfer.setFundTransferId(1L);
		
		Login login = new Login();
		login.setLogInStatus(LogInStatus.LOGGEDIN);
		login.setCustomer(customer);

		List<Transaction> transactions = new ArrayList<>();

		Transaction transaction1 = new Transaction();
		transaction1.setAccountNumber(Long.valueOf(12345));
		transaction1.setFundTransfer(fundTransfer);
		
		Transaction transaction2 = new Transaction();
		transaction2.setAccountNumber(Long.valueOf(12345));
		transaction2.setFundTransfer(fundTransfer);
		transactions.add(transaction1);
		transactions.add(transaction2);
		

		List<TransactionResponseDto> transactionResponseList = new ArrayList<>();
		TransactionResponseDto transactionDto= new TransactionResponseDto();
		transactionDto.setAccountNumber(account.getAccountNumber());

		transactionResponseList.add(transactionDto);
		
		AccountDetails accountDetails =new AccountDetails();
		accountDetails.setAccountbalance(account.getAccountBalance());
		accountDetails.setAccountNumber(account.getAccountNumber());
		accountDetails.setTransactionList(transactionResponseList);
		
		Mockito.when(accountRepository.findByAccountNumber(anyLong())).thenReturn(account);
		Mockito.when(transactionRepository.findByAccountNumber(anyLong())).thenReturn(transaction1);
		Mockito.when(loginRepository.findByCustomerCustomerId(anyLong())).thenReturn(login);
		Mockito.when(
				transactionRepository.findTop10ByAccountNumberOrderByTransactionDateDesc(account.getAccountNumber()))
				.thenReturn(transactions);
		Mockito.when(fundTransferRepository.findByFundTransferId(1L)).thenReturn(fundTransfer);
		
		assertEquals(accountDetails.getAccountNumber(),accountServiceImpl.retrieveTransactions(12345L).getAccountNumber() );
		
	}

}
