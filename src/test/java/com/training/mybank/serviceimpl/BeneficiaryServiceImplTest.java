package com.training.mybank.serviceimpl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.anyLong;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.training.mybank.dto.BeneficiaryAccountResponse;
import com.training.mybank.entity.Account;
import com.training.mybank.entity.BeneficiaryAccount;
import com.training.mybank.entity.Customer;
import com.training.mybank.exception.AccountMisMatchException;
import com.training.mybank.exception.AccountNotFound;
import com.training.mybank.exception.BeneficiaryAccountNotFound;
import com.training.mybank.exception.CustomerNotFound;
import com.training.mybank.repository.AccountRepository;
import com.training.mybank.repository.BeneficiaryAccountRepository;
import com.training.mybank.repository.CustomerRepository;
import com.training.mybank.service.impl.BeneficiaryServiceImpl;

@ExtendWith(SpringExtension.class)
class BeneficiaryServiceImplTest {

	@Mock
	BeneficiaryAccountRepository beneficiaryAccountRepository;

	@Mock
	CustomerRepository customerRepository;

	@Mock
	AccountRepository accountRepository;

	@InjectMocks
	BeneficiaryServiceImpl beneficiaryServiceImpl;

	@Test
	void testGetAllBeneficiaryForCustomerNotFound() {

		Mockito.when(customerRepository.findByCustomerId(anyLong())).thenReturn(null);
		assertThrows(CustomerNotFound.class, () -> {
			beneficiaryServiceImpl.getAllBeneficiary(121212l, 1l);
		});
	}

	@Test
	void testGetAllBeneficiaryForAccountNotFound() {

		Customer customer = new Customer();
		customer.setCustomerId(1l);

		Mockito.when(customerRepository.findByCustomerId(anyLong())).thenReturn(customer);
		Mockito.when(accountRepository.findByAccountNumber(anyLong())).thenReturn(null);

		assertThrows(AccountNotFound.class, () -> {
			beneficiaryServiceImpl.getAllBeneficiary(121212L, 1l);
		});
	}

	@Test
	void testGetAllBeneficiaryForAccountMissMatch() {
		Customer customer = new Customer();
		customer.setCustomerId(1l);

		Account account = new Account();
		account.setCustomer(customer);
		account.setAccountNumber(121212L);

		Mockito.when(customerRepository.findByCustomerId(anyLong())).thenReturn(customer);
		Mockito.when(accountRepository.findByAccountNumber(anyLong())).thenReturn(account);

		assertThrows(AccountMisMatchException.class, () -> {
			beneficiaryServiceImpl.getAllBeneficiary(121212L, 2l);
		});

	}

	@Test
	void testForNoBeneficiaryAccount() {
		Customer customer = new Customer();
		customer.setCustomerId(1l);

		Account account = new Account();
		account.setCustomer(customer);
		account.setAccountNumber(121212L);
		List<BeneficiaryAccount> accounts = new ArrayList<BeneficiaryAccount>();

		Mockito.when(beneficiaryAccountRepository.findAllByAccountAccountId(anyLong())).thenReturn(accounts);

		Mockito.when(customerRepository.findByCustomerId(anyLong())).thenReturn(customer);
		Mockito.when(accountRepository.findByAccountNumber(anyLong())).thenReturn(account);
		assertThrows(BeneficiaryAccountNotFound.class, () -> {
			beneficiaryServiceImpl.getAllBeneficiary(121212L, 1l);
		});
	}
	
	@Test
	void testNoBeneficiaryAccount() {
		Customer customer = new Customer();
		customer.setCustomerId(1l);

		Account account = new Account();
		account.setCustomer(customer);
		account.setAccountNumber(1L);
		
		Account beneficiary = new Account();
		account.setCustomer(customer);
		account.setAccountNumber(2L);
		
		List<BeneficiaryAccount> accounts = new ArrayList<BeneficiaryAccount>();
		BeneficiaryAccount beneficiaryAccount= new BeneficiaryAccount();
		beneficiaryAccount.setAccount(account);
		beneficiaryAccount.setAccountNumber(2L);
		Mockito.when(beneficiaryAccountRepository.findAllByAccountAccountId(anyLong())).thenReturn(accounts);

		Mockito.when(customerRepository.findByCustomerId(anyLong())).thenReturn(customer);
		Mockito.when(accountRepository.findByAccountNumber(anyLong())).thenReturn(account);
		assertEquals(0,accounts.size());
		assertThrows(BeneficiaryAccountNotFound.class, () -> {
			beneficiaryServiceImpl.getAllBeneficiary(121212L, 1l);
		});
	}

	
	@Test
	void testSuccess() {
		Customer customer = new Customer();
		customer.setCustomerId(1l);

		Account account = new Account();
		account.setCustomer(customer);
		account.setAccountId(1L);
		account.setAccountNumber(11L);
		
		Account beneficiary = new Account();
		beneficiary.setCustomer(customer);
		beneficiary.setAccountId(2L);
		account.setAccountNumber(22L);
		
		List<BeneficiaryAccount> accounts = new ArrayList<BeneficiaryAccount>();
		BeneficiaryAccount beneficiaryAccount= new BeneficiaryAccount();
		beneficiaryAccount.setAccount(account);
		beneficiaryAccount.setAccountNumber(beneficiary.getAccountNumber());
		
		accounts.add(beneficiaryAccount);

		Mockito.when(beneficiaryAccountRepository.findAllByAccountAccountId(anyLong())).thenReturn(accounts);
		Mockito.when(customerRepository.findByCustomerId(anyLong())).thenReturn(customer);
		Mockito.when(accountRepository.findByAccountNumber(anyLong())).thenReturn(account);
		
		List<BeneficiaryAccountResponse> accountResponses = new ArrayList<BeneficiaryAccountResponse>();
		BeneficiaryAccountResponse  beneficiaryAccountResponse = new BeneficiaryAccountResponse();
		beneficiaryAccountResponse.setAccountNumber(beneficiaryAccount.getAccountNumber());
		beneficiaryAccountResponse.setBeneficiaryName(beneficiaryAccount.getAccountHolderName());
		accountResponses.add(new BeneficiaryAccountResponse());
		List<BeneficiaryAccountResponse> result= beneficiaryServiceImpl.getAllBeneficiary(account.getAccountNumber(),
				customer.getCustomerId());
		assertEquals(accountResponses.size(), result.size());
		
	}

}
