package com.training.mybank.serviceimpl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import com.training.mybank.dto.LoginDto;
import com.training.mybank.dto.ResponseDto;
import com.training.mybank.entity.Customer;
import com.training.mybank.entity.LogInStatus;
import com.training.mybank.entity.Login;
import com.training.mybank.exception.CustomerAlreadyLoggedIn;
import com.training.mybank.exception.CustomerNotFoundException;
import com.training.mybank.exception.InvalidCredentialException;
import com.training.mybank.repository.CustomerRepository;
import com.training.mybank.repository.LoginRepository;
import com.training.mybank.service.impl.LoginServiceImpl;

@ExtendWith(MockitoExtension.class)
class LoginServiceImplTest {

	@Mock
	private CustomerRepository customerRepository;

	@Mock
	private LoginRepository loginRepository;

	@InjectMocks
	private LoginServiceImpl loginService;

	@Test
	void testInvalidCredentialException() {

		Login login = new Login();
		LoginDto loginDto = new LoginDto(1L, "Chaitra@514");
		Customer customer = new Customer();
		customer.setCustomerId(1L);
		login.setCustomer(customer);
		login.setPassword("hdjfsgsyu");
		login.setLogInStatus(LogInStatus.LOGGEDOUT);
		when(customerRepository.findByCustomerId(loginDto.customerId())).thenReturn(customer);
		when(loginRepository.findByCustomerCustomerId(loginDto.customerId())).thenReturn(login);
		InvalidCredentialException exception = assertThrows(InvalidCredentialException.class,
				() -> loginService.login(loginDto));
		assertEquals("Invalid Credentials", exception.getMessage());

	}

	@Test
	void testValidLogin() {
		LoginDto loginDto = new LoginDto(1L, "Chaitra@514");

		Customer customer = new Customer();
		customer.setCustomerId(1L);
		when(customerRepository.findByCustomerId(loginDto.customerId())).thenReturn(customer);
		Login login = new Login();
		login.setCustomer(customer);
		login.setPassword("Chaitra@514");
		login.setLogInStatus(LogInStatus.LOGGEDOUT);
		when(loginRepository.findByCustomerCustomerId(loginDto.customerId())).thenReturn(login);

		ResponseDto response = loginService.login(loginDto);

		assertEquals("Logged In successfully", response.message());
		assertEquals(200, response.code());
	}

	@Test
	void testCustomerNotFound() {
		LoginDto loginDto = new LoginDto(1L, "Chaitra@514");
		Customer customer = null;

		when(customerRepository.findByCustomerId(loginDto.customerId())).thenReturn(customer);
		CustomerNotFoundException exception = assertThrows(CustomerNotFoundException.class,
				() -> loginService.login(loginDto));
		assertEquals("Customer not found", exception.getMessage());

	}

	@Test
	void testCustomerAlreadyLoggedIn1() {
		Customer customer = new Customer();
		customer.setCustomerId(1L);
		customer.setFirstName("vinay");

		Login login = new Login();
		login.setPassword("vinay@123");
		login.setCustomer(customer);
		login.setLogInStatus(LogInStatus.LOGGEDIN);
		LoginDto dto = new LoginDto(1l, "vinay@123");

		Mockito.when(customerRepository.findByCustomerId(anyLong())).thenReturn(customer);
		Mockito.when(loginRepository.findByCustomerCustomerId(anyLong())).thenReturn(login);

		assertThrows(CustomerAlreadyLoggedIn.class, () -> loginService.login(dto));

	}

}