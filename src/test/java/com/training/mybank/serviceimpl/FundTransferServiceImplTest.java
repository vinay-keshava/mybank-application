package com.training.mybank.serviceimpl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.anyLong;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.training.mybank.dto.FundTransferDto;
import com.training.mybank.dto.OtpRequest;
import com.training.mybank.dto.OtpResponse;
import com.training.mybank.dto.OtpVerification;
import com.training.mybank.dto.ResponseDto;
import com.training.mybank.entity.Account;
import com.training.mybank.entity.BeneficiaryAccount;
import com.training.mybank.entity.FundTransfer;
import com.training.mybank.entity.Transaction;
import com.training.mybank.entity.TransactionType;
import com.training.mybank.exception.AccountNotFound;
import com.training.mybank.exception.InsufficientBalanceExcpetion;
import com.training.mybank.exception.OtpVerificationFailed;
import com.training.mybank.feing.OtpFiegnClient;
import com.training.mybank.repository.AccountRepository;
import com.training.mybank.repository.BeneficiaryAccountRepository;
import com.training.mybank.repository.FundTransferRepository;
import com.training.mybank.repository.TransactionRepository;
import com.training.mybank.service.impl.FundTransferServiceImpl;

@ExtendWith(SpringExtension.class)
class FundTransferServiceImplTest {

	@InjectMocks
	private FundTransferServiceImpl fundTransferServiceImpl;

	@Mock
	private AccountRepository accountRepository;

	@Mock
	private FundTransferRepository fundTransferRepository;

	@Mock
	private TransactionRepository transactionRepository;

	@Mock
	private BeneficiaryAccountRepository beneficiaryAccountRepository;

	@Mock
	private OtpFiegnClient fiegnClient;

	@Test
	void testfundTransfer_FromAccountNotFound() {

		Mockito.when(accountRepository.findByAccountNumber(Mockito.anyLong())).thenReturn(null);

		FundTransferDto dto = new FundTransferDto();
		AccountNotFound exception = assertThrows(AccountNotFound.class,
				() -> fundTransferServiceImpl.fundTransfer(dto));
		assertEquals("from account not found", exception.getMessage());
	}

	@Test
	void testfundTransfer_ToAccountNotFound() {

		Account account = new Account();
		account.setAccountNumber(653872190l);
		Mockito.when(accountRepository.findByAccountNumber(Mockito.anyLong())).thenReturn(account);
		FundTransferDto dto = new FundTransferDto();
		dto.setFromAccountNumber(653872190l);
		AccountNotFound exception = assertThrows(AccountNotFound.class,
				() -> fundTransferServiceImpl.fundTransfer(dto));
		assertEquals("To account not found", exception.getMessage());
	}

	@Test
	void testSuccess() {
		FundTransferDto dto = new FundTransferDto();
		dto.setFromAccountNumber(6538721901L);
		dto.setToAccountNumber(6538721902L);
		dto.setTransactionType(TransactionType.DEBIT);
		dto.setAmount(50.00);

		Account fromAccount = new Account();
		fromAccount.setAccountNumber(6538721901L);
		fromAccount.setAccountBalance(500.00);

		Account toAccount = new Account();
		toAccount.setAccountId(2L);
		toAccount.setAccountNumber(6538721902L);
		toAccount.setAccountBalance(5000.00);

		List<BeneficiaryAccount> accounts = new ArrayList<>();
		BeneficiaryAccount beneficiaryAccount = new BeneficiaryAccount();
		beneficiaryAccount.setAccountHolderName("darshan");
		beneficiaryAccount.setAccountNumber(6538721902L);

		BeneficiaryAccount beneficiaryAccount1 = new BeneficiaryAccount();
		beneficiaryAccount1.setAccountHolderName("vinay");
		beneficiaryAccount1.setAccountNumber(3423434l);
		accounts.add(beneficiaryAccount1);
		accounts.add(beneficiaryAccount);

		Mockito.when(accountRepository.findByAccountNumber(dto.getFromAccountNumber())).thenReturn(fromAccount);
		Mockito.when(accountRepository.findByAccountNumber(dto.getToAccountNumber())).thenReturn(toAccount);
		Mockito.when(beneficiaryAccountRepository.findAllByAccountAccountId(anyLong())).thenReturn(accounts);

		ResponseDto dto2 = new ResponseDto("Validate the OTP", 200);
		assertEquals(dto2, fundTransferServiceImpl.fundTransfer(dto));

	}
	
	@Test
		void testSuccess2() {
			FundTransferDto dto = new FundTransferDto();
			dto.setFromAccountNumber(6538721901L);
			dto.setToAccountNumber(6538721902L);
			dto.setTransactionType(TransactionType.DEBIT);
			dto.setAmount(50.00);
	 
			Account fromAccount = new Account();
			fromAccount.setAccountNumber(6538721901L);
			fromAccount.setAccountBalance(500.00);
	 
			Account toAccount = new Account();
			toAccount.setAccountId(2L);
			toAccount.setAccountNumber(6538721902L);
			toAccount.setAccountBalance(5000.00);
	 
			List<BeneficiaryAccount> accounts = new ArrayList<>();
			BeneficiaryAccount beneficiaryAccount = new BeneficiaryAccount();
			beneficiaryAccount.setAccountHolderName("darshan");
			beneficiaryAccount.setAccountNumber(6538721902L);
	 
			BeneficiaryAccount beneficiaryAccount1 = new BeneficiaryAccount();
			beneficiaryAccount1.setAccountHolderName("vinay");
			beneficiaryAccount1.setAccountNumber(3423434l);
			accounts.add(beneficiaryAccount1);
			accounts.add(beneficiaryAccount);
	 
			OtpRequest otpRequest=new OtpRequest();
			otpRequest.setTo("1234");
			
			OtpResponse otpResponse= new OtpResponse();
			otpResponse.setOtp("1234");
			
			Mockito.when(accountRepository.findByAccountNumber(dto.getFromAccountNumber())).thenReturn(fromAccount);
			Mockito.when(accountRepository.findByAccountNumber(dto.getToAccountNumber())).thenReturn(toAccount);
			Mockito.when(beneficiaryAccountRepository.findAllByAccountAccountId(anyLong())).thenReturn(accounts);
			Mockito.when(fiegnClient.getOtp(otpRequest)).thenReturn(otpResponse);
			FundTransfer fundTransfer=new FundTransfer();
			fundTransfer.setAccountNumber(fromAccount.getAccountNumber());
			
			ResponseDto responseDto=new ResponseDto("Validate the OTP", 200);
			ResponseDto dto2=fundTransferServiceImpl.fundTransfer(dto);
			assertEquals("Validate the OTP", dto2.message());
			
		}
//	@Test
//	void testSuccess3() {
//		
//		FundTransferDto dto = new FundTransferDto();
//		dto.setFromAccountNumber(6538721901L);
//		dto.setToAccountNumber(6538721902L);
//		dto.setTransactionType(TransactionType.DEBIT);
//		dto.setAmount(50.00);
// 
//		Account fromAccount = new Account();
//		fromAccount.setAccountNumber(6538721901L);
//		fromAccount.setAccountBalance(500.00);
// 
//		Account toAccount = new Account();
//		toAccount.setAccountId(2L);
//		toAccount.setAccountNumber(6538721902L);
//		toAccount.setAccountBalance(500.00);
// 
//		List<BeneficiaryAccount> accounts = new ArrayList<>();
//		BeneficiaryAccount beneficiaryAccount = new BeneficiaryAccount();
//		beneficiaryAccount.setAccountHolderName("darshan");
//		beneficiaryAccount.setAccountNumber(6538721902L);
//	
//		accounts.add(beneficiaryAccount);
// 
//		Mockito.when(accountRepository.findByAccountNumber(dto.getFromAccountNumber())).thenReturn(fromAccount);
//		Mockito.when(accountRepository.findByAccountNumber(dto.getToAccountNumber())).thenReturn(toAccount);
//		Mockito.when(beneficiaryAccountRepository.findAllByAccountAccountId(fromAccount.getAccountId())).thenReturn(accounts);
//		
//		OtpRequest otpRequest=new OtpRequest();
//		otpRequest.setTo("1234");
//		
//		OtpResponse otpResponse= new OtpResponse();
//		otpResponse.setOtp("1234");
//		Mockito.when(fiegnClient.getOtp(otpRequest)).thenReturn(otpResponse);
//		ResponseDto responseDto=fundTransferServiceImpl.fundTransfer(dto);
//		assertEquals("Validate the OTP", responseDto.message());
//	}
	
	@Test
	void testInsufficientBalance() {
		
		FundTransferDto dto = new FundTransferDto();
		dto.setFromAccountNumber(6538721901L);
		dto.setToAccountNumber(6538721902L);
		dto.setTransactionType(TransactionType.DEBIT);
		dto.setAmount(50000.00);
 
		Account fromAccount = new Account();
		fromAccount.setAccountNumber(6538721901L);
		fromAccount.setAccountBalance(500.00);
 
		Account toAccount = new Account();
		toAccount.setAccountId(2L);
		toAccount.setAccountNumber(6538721902L);
		toAccount.setAccountBalance(500.00);
 
		List<BeneficiaryAccount> accounts = new ArrayList<>();
		BeneficiaryAccount beneficiaryAccount = new BeneficiaryAccount();
		beneficiaryAccount.setAccountHolderName("darshan");
		beneficiaryAccount.setAccountNumber(6538721902L);
	
		accounts.add(beneficiaryAccount);
 
		Mockito.when(accountRepository.findByAccountNumber(dto.getFromAccountNumber())).thenReturn(fromAccount);
		Mockito.when(accountRepository.findByAccountNumber(dto.getToAccountNumber())).thenReturn(toAccount);
		Mockito.when(beneficiaryAccountRepository.findAllByAccountAccountId(fromAccount.getAccountId())).thenReturn(accounts);
		
		assertThrows(InsufficientBalanceExcpetion.class, () -> {
			fundTransferServiceImpl.fundTransfer(dto);
		});
	}
	
	@Test
	void testValidateOtpSuccessForCredit() {
 
		OtpVerification otpVerification = new OtpVerification();
		otpVerification.setFundTransferId(1L);
		otpVerification.setOtp("1234");
 
		FundTransfer fundTransfer = new FundTransfer();
		fundTransfer.setOtpToken("1234");
		fundTransfer.setFundTransferId(1l);
		fundTransfer.setAccountNumber(5654645645l);
		fundTransfer.setTransactionAmount(100.00);
		fundTransfer.setBeneficiaryAccountNumber(2343243l);
		fundTransfer.setTransactionType(TransactionType.CREDIT);
 
		Account fromAccount = new Account();
		fromAccount.setAccountNumber(5654645645l);
		fromAccount.setAccountBalance(10000.00);
 
		Account toAccount = new Account();
		toAccount.setAccountNumber(2343243l);
		toAccount.setAccountBalance(10000.00);
		
		Mockito.when(accountRepository.findByAccountNumber(fundTransfer.getAccountNumber())).thenReturn(fromAccount);
		Mockito.when(accountRepository.findByAccountNumber(fundTransfer.getBeneficiaryAccountNumber()))
				.thenReturn(toAccount);
		Mockito.when(fundTransferRepository.findByFundTransferId(otpVerification.getFundTransferId())).thenReturn(fundTransfer);
		
		Transaction transaction1 = new Transaction();
		Transaction transaction2 = new Transaction();
 
		Mockito.when(accountRepository.save(toAccount)).thenReturn(toAccount);
		Mockito.when(accountRepository.save(fromAccount)).thenReturn(fromAccount);
		Mockito.when(transactionRepository.save(transaction1)).thenReturn(transaction1);
		Mockito.when(transactionRepository.save(transaction2)).thenReturn(transaction2);
 
		
		ResponseDto dto = fundTransferServiceImpl.validateOtp(otpVerification);
 
		assertEquals("Transaction Successfull", dto.message());
		assertEquals(200, dto.code());
	}
	
	@Test
	void testValidateOtpSuccessForDebit() {
 
		OtpVerification otpVerification = new OtpVerification();
		otpVerification.setFundTransferId(1L);
		otpVerification.setOtp("1234");
 
		FundTransfer fundTransfer = new FundTransfer();
		fundTransfer.setOtpToken("1234");
		fundTransfer.setFundTransferId(1l);
		fundTransfer.setAccountNumber(5654645645l);
		fundTransfer.setTransactionAmount(100.00);
		fundTransfer.setBeneficiaryAccountNumber(2343243l);
		fundTransfer.setTransactionType(TransactionType.DEBIT);
 
		Account fromAccount = new Account();
		fromAccount.setAccountNumber(5654645645l);
		fromAccount.setAccountBalance(10000.00);
 
		Account toAccount = new Account();
		toAccount.setAccountNumber(2343243l);
		toAccount.setAccountBalance(10000.00);
		
		Mockito.when(accountRepository.findByAccountNumber(fundTransfer.getAccountNumber())).thenReturn(fromAccount);
		Mockito.when(accountRepository.findByAccountNumber(fundTransfer.getBeneficiaryAccountNumber()))
				.thenReturn(toAccount);
		Mockito.when(fundTransferRepository.findByFundTransferId(otpVerification.getFundTransferId())).thenReturn(fundTransfer);
		
		Transaction transaction1 = new Transaction();
		Transaction transaction2 = new Transaction();
 
		Mockito.when(accountRepository.save(toAccount)).thenReturn(toAccount);
		Mockito.when(accountRepository.save(fromAccount)).thenReturn(fromAccount);
		Mockito.when(transactionRepository.save(transaction1)).thenReturn(transaction1);
		Mockito.when(transactionRepository.save(transaction2)).thenReturn(transaction2);
 
		
		ResponseDto dto = fundTransferServiceImpl.validateOtp(otpVerification);
 
		assertEquals("Transaction Successfull", dto.message());
		assertEquals(200, dto.code());
	}
	
	@Test
	void testForInvalidOtp()
	{
		OtpVerification otpVerification = new OtpVerification();
		otpVerification.setFundTransferId(1L);
		otpVerification.setOtp("23434");
 
		FundTransfer fundTransfer = new FundTransfer();
		fundTransfer.setOtpToken("1234");
		fundTransfer.setFundTransferId(1l);
		fundTransfer.setAccountNumber(5654645645l);
		fundTransfer.setBeneficiaryAccountNumber(2343243l);
		fundTransfer.setTransactionType(TransactionType.DEBIT);
		
		Mockito.when(fundTransferRepository.findByFundTransferId(otpVerification.getFundTransferId())).thenReturn(fundTransfer);
	
		assertThrows(OtpVerificationFailed.class, () -> {
			fundTransferServiceImpl.validateOtp(otpVerification);
		});
	
	}

}