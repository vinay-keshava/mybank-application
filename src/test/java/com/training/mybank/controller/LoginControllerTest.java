package com.training.mybank.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.training.mybank.dto.LoginDto;
import com.training.mybank.dto.ResponseDto;
import com.training.mybank.service.LoginService;

@ExtendWith(SpringExtension.class)
class LoginControllerTest {


		    @Mock
		    LoginService loginService;
	 
		    @InjectMocks
		    LoginController loginController;
		    
		    @Test
			void testSucess() {
				LoginDto loginDto = new LoginDto(1L, "CHAITRA");
				Mockito.when(loginService.login(any(LoginDto.class))).thenReturn(new ResponseDto( "Logged In successfully",200));
				ResponseEntity<ResponseDto> response = loginController.customerLogin(loginDto);
				assertEquals(HttpStatusCode.valueOf(200), response.getStatusCode());
			}
	 
}
