package com.training.mybank.controller;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.anyLong;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.training.mybank.dto.BeneficiaryAccountResponse;
import com.training.mybank.service.impl.BeneficiaryServiceImpl;

@ExtendWith(SpringExtension.class)
class BeneficiaryControllerTest {

	@Mock
	BeneficiaryServiceImpl beneficiaryServiceImpl;

	@InjectMocks
	BeneficiaryController beneficiaryController;

	@Test
	void testGetBeneficiaryDetails() {
		List<BeneficiaryAccountResponse> accountResponses = new ArrayList<>();

		BeneficiaryAccountResponse beneficiaryAccountResponse1 = new BeneficiaryAccountResponse();
		beneficiaryAccountResponse1.setAccountNumber(1L);
		beneficiaryAccountResponse1.setBeneficiaryName("Vinay");

		BeneficiaryAccountResponse beneficiaryAccountResponse2 = new BeneficiaryAccountResponse();
		beneficiaryAccountResponse1.setAccountNumber(2L);
		beneficiaryAccountResponse1.setBeneficiaryName("Darshan");

		accountResponses.add(beneficiaryAccountResponse1);
		accountResponses.add(beneficiaryAccountResponse2);

		Mockito.when(beneficiaryServiceImpl.getAllBeneficiary(anyLong(), anyLong())).thenReturn(accountResponses);

		ResponseEntity<List<BeneficiaryAccountResponse>> result = beneficiaryController.getBeneficiaryDetails(1234l,
				1l);

		assertNotNull(result);

	}

}
