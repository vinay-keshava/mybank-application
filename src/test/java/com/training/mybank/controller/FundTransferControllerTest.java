package com.training.mybank.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.training.mybank.dto.FundTransferDto;
import com.training.mybank.dto.OtpVerification;
import com.training.mybank.dto.ResponseDto;
import com.training.mybank.entity.TransactionType;
import com.training.mybank.service.FundTransferService;

@ExtendWith(SpringExtension.class)
class FundTransferControllerTest {

	@Mock
	FundTransferService fundTransferService;

	@InjectMocks
	FundTransferController fundTransferController;

	@Test
	void testfundTransferController() {
		FundTransferDto dto = new FundTransferDto();
		dto.setAmount(200.00);
		dto.setFromAccountNumber(1L);
		dto.setToAccountNumber(2L);
		dto.setTransactionType(TransactionType.CREDIT);
		ResponseDto dto2 = new ResponseDto("Validate the OTP", 201);
		Mockito.when(fundTransferService.fundTransfer(dto)).thenReturn(dto2);

		ResponseEntity<ResponseDto> result = fundTransferController.fundTransfer(dto);
		assertEquals("Validate the OTP", result.getBody().message());
	}
	
	@Test
	void testVerifyTransaction() {
		
		ResponseDto dto=new ResponseDto("transaction successfull", 201);
		
		OtpVerification verification=new OtpVerification();
		verification.setFundTransferId(1L);
		verification.setOtp("1234");
		
		Mockito.when(fundTransferService.validateOtp(verification)).thenReturn(dto);
		
		ResponseEntity<ResponseDto> result=fundTransferController.verifyTransaction(verification);
		 assertEquals("transaction successfull", result.getBody().message());
	}

}
